//
//  AppointmentModel.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 10/4/19.
//  Copyright © 2019 Christopher Ching. All rights reserved.
//

import UIKit
import Foundation
 
class AppointmentModel: NSObject {
    
    //properties
    var idAppointment: String!
    var date: String!
    var idService_id: String!
    //empty constructor
    
    override init()
    {
        
    }
    
    //construct with @name, @address, @latitude, and @longitude parameters
    
    init(date: String, idAppointment: String, idService_id: String) {
        self.date = date
        self.idAppointment = idAppointment
        self.idService_id = idService_id
    }
    
    
    //prints object's current state
    override var description: String {
        return "date: \(date!), idAppointment: \(idAppointment!), idService_id\(idService_id!)"
        
    }
}
