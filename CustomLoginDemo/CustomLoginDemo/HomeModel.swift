//
//  HomeModel.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 10/2/19.
//  Copyright © 2019 Christopher Ching. All rights reserved.
//

import UIKit
import Foundation
 
protocol HomeModelProtocol: class {
    func itemsDownloaded(items: NSArray)
}
 
 
class HomeModel: NSObject, URLSessionDataDelegate {
    
    //properties
    
    weak var delegate: HomeModelProtocol!
    
    var data = Data()
    
    let urlPath: String = "https://imocitas.000webhostapp.com/api/citasCorrespondientesACarnet.php"
    
    func parseJSON(_ data:Data) {
        
        var jsonResult = NSArray()
        
        do{
            jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.allowFragments) as! NSArray
            
        } catch let error as NSError {
            print(error)
            
        }
        
        var jsonElement = NSDictionary()
        let appointments = NSMutableArray()
        
        for i in 0 ..< jsonResult.count
        {
            
            jsonElement = jsonResult[i] as! NSDictionary
            
            let appointment = AppointmentModel()
            
            //the following insures none of the JsonElement values are nil through optional binding
            if let date = jsonElement["date"] as? String,
                let idAppointment = jsonElement["idAppointment"] as? String, let idService_id = jsonElement["idService_id"] as? String
            {
                
                appointment.date = date
                appointment.idAppointment = idAppointment
                appointment.idService_id = idService_id
            }
            appointments.add(appointment)
            
        }
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            self.delegate.itemsDownloaded(items: appointments)
            
        })
    }
    
    func downloadItems(carnet: String!, previous: Bool) {
        //let url: URL = URL(string: urlPath)!
        /*
        let defaultSession = Foundation.URLSession(configuration: URLSessionConfiguration.default)*/
        var requestURL:URL!
        if previous == false {
            //created NSURL
            requestURL = URL(string: urlPath)
        }else{
            requestURL = URL(string: "https://imocitas.000webhostapp.com/api/citasPreviasCorrespondientesACarnet.php")
        }
        
        
        //creating NSMutableURLRequest
        let request = NSMutableURLRequest(url: requestURL!)
        
        //setting the method to post
        request.httpMethod = "POST"
        
        
        //creating the post parameter by concatenating the keys and values from text field
        let postParameters = "carnet="+carnet!;
        /*print(postParameters)*/
        
        //adding the parameters to request body
        request.httpBody = postParameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){
        data, response, error in
            
            if error != nil {
                print("Failed to download data")
            }else {
                print("Data downloaded")
                self.parseJSON(data!)
            }
            
        }
        
        task.resume()
    }
}



