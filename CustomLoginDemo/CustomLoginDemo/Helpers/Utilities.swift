import Foundation
import UIKit

class Utilities {
    
    static func styleTextField(_ textfield:UITextField) {
        
        // Create the bottom line
        let bottomLine = CALayer()
        
        bottomLine.frame = CGRect(x: 0, y: textfield.frame.height - 2, width: textfield.frame.width, height: 2)
        
        bottomLine.backgroundColor = UIColor(hue: 0, saturation: 1, brightness: 0.58, alpha: 1.0).cgColor
        
        // Remove border on text field
        textfield.borderStyle = .none
        
        // Add the line to the text field
        textfield.layer.addSublayer(bottomLine)
        
    }
    
    static func styleFilledButton(_ button:UIButton) {
        // Filled rounded corner style NEW STYLE
        button.backgroundColor = UIColor(red:0.53, green:0.10, blue:0.06, alpha:1.0)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.setTitleColor(.white, for: UIControl.State.normal)
        button.clipsToBounds = true
        //button.tintColor = UIColor.white
    }
    
    static func styleHollowButton(_ button:UIButton) {
       // Hollow rounded corner style
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor(hue: 0, saturation: 1, brightness: 0.58, alpha: 1.0).cgColor
        button.layer.cornerRadius = 5.0
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.tintColor = UIColor(hue: 0, saturation: 1, brightness: 0.58, alpha: 1.0)
    }
    
    static func isPasswordValid(_ password : String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
}
