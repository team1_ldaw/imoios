//
//  ManualViewController.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 23/10/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation

class ManualViewController: UIViewController {
    @IBOutlet weak var botonGoBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func goBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
