//
//  CarnetViewController.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 9/26/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import UserNotifications

class CarnetViewController: UIViewController {
    
    @IBOutlet weak var carnetLabel: UILabel!
    
    @IBOutlet weak var carnetTextField: UITextField!
    
    @IBOutlet weak var preguntasFrecuentes: UIButton!
    
    @IBOutlet weak var validarCarnetButton: UIButton!
    
    @IBOutlet weak var avisoDePrivacidad: UIButton!
    
    
    @IBOutlet weak var information: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    
    @IBOutlet weak var doctores: UIButton!
    
    var carnetDelUsuario : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpElements()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler:  {didAllow, error in})
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    let urlPath: String = "https://imocitas.000webhostapp.com/carnets.php"
    
    func setUpElements() {
        // Hide the error label
        errorLabel.alpha = 0
        // Style the elements
        Utilities.styleTextField(carnetTextField)
        Utilities.styleFilledButton(validarCarnetButton)
        Utilities.styleHollowButton(information)
        Utilities.styleHollowButton(doctores)
        Utilities.styleHollowButton(preguntasFrecuentes)
        Utilities.styleHollowButton(avisoDePrivacidad)
    }
    
    // Check the fields and validate that the data is correct. If everything is correct, this method returns nil. Otherwise, it returns the error message
    func validateFields() -> String? {
        
        // Check that all fields are filled in
        if carnetTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""  {
            return "Por favor llene todos los campos."
        }
        
        return nil
    }
    
    var carnetLeido: String!
    
    @IBAction func validarCarnet(_ sender: Any) {
        
        // Validate the fields
        
        let error = validateFields()
        
        if error != nil {
            
            // There's something wrong with the fields, show error message
            showError(error!)
        }
        else {
            
            // Create cleaned versions of the data
            let carnet = carnetTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            carnetDelUsuario = carnet
            
            // Valdidar que exista el carnet
            downloadItems(carnet: carnetDelUsuario)
            
            print("entering")
            //print("CARNET IMPRESO: "+carnetLeido)*/
            
            
            
            
        }
    }
    
    func aceptarCarnet () {
        if carnetLeido == nil {
            print("No hay")
            self.showError("Carnet inválido")
        } else {
            print("CARNET IMPRESO: "+carnetLeido)
            self.performSegue(withIdentifier: "CarnetAIniciarSesion", sender: nil)
        }
    }
    
    func parseJSON(_ data:Data) {
        print("pasing")
        var jsonResult = NSArray()
        
        do{
            jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.allowFragments) as! NSArray
            
        } catch let error as NSError {
            print(error)
        }
        
        var jsonElement = NSDictionary()
        
        for i in 0 ..< jsonResult.count
        {
            jsonElement = jsonResult[i] as! NSDictionary
            
            //the following insures none of the JsonElement values are nil through optional binding
            if let fetchedCarnet = jsonElement["name"] as? String
            {
                carnetLeido = fetchedCarnet;
                print("carnetLeido: "+carnetLeido)
            }
        }
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            self.aceptarCarnet();
            
        })
    }
    
    func downloadItems(carnet: String!) {
        print("descargando")
        var requestURL:URL!
        
        requestURL = URL(string: urlPath)
        
        //creating NSMutableURLRequest
        let request = NSMutableURLRequest(url: requestURL!)
        
        //setting the method to post
        request.httpMethod = "POST"
        
        //creating the post parameter by concatenating the keys and values from text field
        let postParameters = "carnet="+carnet!;
        
        //adding the parameters to request body
        request.httpBody = postParameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){
        data, response, error in
            
            if error != nil {
                print("Failed to download data")
            }else {
                print("Data downloaded")
                //print(data as Any)
                self.parseJSON(data!)
            }
        }
        task.resume()
        
        
    }
    
    func showError(_ message:String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(!carnetDelUsuario.isEmpty){
            let destVC = segue.destination as! ViewController
            destVC.carnetUsuario = carnetDelUsuario
        }
        
    }
    
}
