//
//  AccountViewController+UI.swift
//  CustomLoginDemo
//
//  Created by Alejandro Gleason on 9/28/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Firebase
import Foundation

/* Here I just define the design components */

extension AccountViewController {
    
    func setupEmailTextField(){
        emailContainer.layer.borderWidth = 1
        emailContainer.layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor
        emailContainer.layer.cornerRadius = 3
        emailContainer.clipsToBounds = true
        
        let services = ["Cataratas","Oftalmo pediatría","Prótesis ocular","Baja visión", "Imagenología", "Retina", "Segmento anterior & córnea", "Glaucoma"]
        
        //emailTextField.borderStyle = .none
        
        let placeholderAttr = NSAttributedString(string: "Nuevo Correo Electrónico", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)])
        
        emailTextField.attributedPlaceholder = placeholderAttr
        emailTextField.textColor = UIColor(red: 99/255, green: 99/255, blue: 99/255, alpha: 1)
        
        passContainer.layer.borderWidth = 1
        passContainer.layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor
        passContainer.layer.cornerRadius = 3
        passContainer.clipsToBounds = true
        
        //emailTextField.borderStyle = .none
        
        let placeholderAttr2 = NSAttributedString(string: "Nueva Contraseña", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)])
        
        passTextField.attributedPlaceholder = placeholderAttr2
        passTextField.textColor = UIColor(red: 99/255, green: 99/255, blue: 99/255, alpha: 1)
    }
    
    
    
    func setupResetButton(){
        resetButton.setTitle("ACTUALIZAR MI CORREO", for: UIControl.State.normal)
        resetButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        resetButton.backgroundColor = UIColor(red:0.53, green:0.10, blue:0.06, alpha:1.0)
        resetButton.layer.cornerRadius = 5
        resetButton.clipsToBounds = true
        resetButton.setTitleColor(.white, for: UIControl.State.normal)
        resetButton.tag = 1
        
        resetPButton.setTitle("ACTUALIZAR MI CONTRASEÑA", for: UIControl.State.normal)
        resetPButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        resetPButton.backgroundColor = UIColor(red:0.53, green:0.10, blue:0.06, alpha:1.0)
        resetPButton.layer.cornerRadius = 5
        resetPButton.clipsToBounds = true
        resetPButton.setTitleColor(.white, for: UIControl.State.normal)
        resetPButton.tag = 2
        
        
    }
}

