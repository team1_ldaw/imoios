//
//  ContactanosViewController.swift
//  CustomLoginDemo
//
//  Created by Alejandro Gleason on 10/16/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation

class ContactanosViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var phone2Button: UIButton!
    @IBOutlet weak var phone1Button: UIButton!
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var webpageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTappedPhone1(_ sender: Any) {
        UIApplication.shared.open(URL(string: "tel://4422290776")! as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func didTappedPhone2(_ sender: Any) {
        UIApplication.shared.open(URL(string: "tel://4422290778")! as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func didTappedMail(_ sender: Any) {
        UIApplication.shared.open(URL(string: "mailto:informacion@imoiap.edu.mx")! as URL, options: [:], completionHandler: nil)
        
    }
    
    @IBAction func didTappedLinkedIn(_ sender: Any) {
        print("Entrare a lnkedin")
        UIApplication.shared.open(URL(string: "https://www.linkedin.com/in/IMOIAP")! as URL, options: [:], completionHandler: nil)
    }

    
    @IBAction func didTappedWebpage(_ sender: Any) {
        //print("Entrare a imoweb")
        UIApplication.shared.open(URL(string: "https://imoiap.com.mx/contacto/")! as URL, options: [:], completionHandler: nil)
    }
}
