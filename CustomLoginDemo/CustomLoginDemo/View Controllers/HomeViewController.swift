import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit
import FBSDKCoreKit
import FBSDKShareKit
import GoogleSignIn
import MapKit
class HomeViewController: UIViewController, GIDSignInDelegate{
    
    var carnetDelUsuario : String!
    
    @IBOutlet weak var myAppointmentsButton: UIButton!
    
    @IBOutlet weak var information: UIButton!
    
    @IBOutlet weak var cerrarSesionButton: UIButton!
    
    @IBOutlet weak var previousAppointmentsButton: UIButton!
    
    @IBOutlet weak var gpsNavigationButton: UIButton!
    
    @IBOutlet weak var verCuenta: UIButton!
    
    @IBOutlet weak var reportarAlgo: UIButton!
    
    @IBOutlet weak var activarCuentaWeb: UIButton!
    
    
    @IBOutlet weak var botonManualUsuario: UIButton!
    override func viewDidLoad() {
        setUpElements()
        super.viewDidLoad()
        print(carnetDelUsuario)
    }
    
    func setUpElements() {
        // Style the elements
        Utilities.styleFilledButton(myAppointmentsButton)
        Utilities.styleFilledButton(previousAppointmentsButton)
        Utilities.styleFilledButton(gpsNavigationButton)
        Utilities.styleFilledButton(information)
        Utilities.styleFilledButton(botonManualUsuario)
        Utilities.styleFilledButton(verCuenta)
        Utilities.styleFilledButton(reportarAlgo)
        Utilities.styleFilledButton(activarCuentaWeb)
    }
    
    @IBAction func myAppointmentsTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "HomeAAppointments", sender: nil)
    }
    
    
    @IBAction func clickActivarcuenta(_ sender: Any) {
        print("Entrar a activar cuenta")
        UIApplication.shared.open(URL(string: "https://imocita.herokuapp.com/register/?car="+carnetDelUsuario)! as URL, options: [:], completionHandler: nil)
    }
    
    
    @IBAction func handleLogOut(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: "¿Estas seguro que deseas salir?", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Cerrar sesión", style: .destructive, handler: { (_) in
            self.signOut()
        }))
        alertController.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    
    func signOut() {
        do {
            try Auth.auth().signOut()
                LoginManager().logOut()
                GIDSignIn.sharedInstance().signOut()

                let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: type(of: self)))
                
                guard let viewController = storyboard.instantiateInitialViewController() else {
                    return
                }
                let presentedViewController = view.window?.rootViewController?.presentedViewController
                presentedViewController?.present(viewController, animated: true)
                view.window?.rootViewController?.dismiss(animated: false)
                view.window?.rootViewController = viewController
            } catch let error {
                print("Failed to sign out with error..", error)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        //...
    }
    
    // START [FUNCTION] for gps navigation
    @IBAction func gpsNavigation(_ sender: Any) {
        let alertGpsNav = UIAlertController(title: "¡Podemos llevarte hasta el IMO!", message: "Da click en la opcion de tu preferencia para continuar.", preferredStyle: .alert)
        
        alertGpsNav.addAction(UIAlertAction(title: "Waze", style: .default, handler: { action in
            //Redirect user to waze app
            //Note: For redirection, the app must be installed
            self.openWaze()
        }))
        alertGpsNav.addAction(UIAlertAction(title: "Maps", style: .default, handler: { action in
            //Redirect user to maps app
            //Note: For redirection, the app must be installed
            self.openMaps()
        }))
        self.present(alertGpsNav, animated: true)
    }
    func openWaze() {
//        let latitude = Double("20.57")!
//        let longitude = Double("-100.3653")!
//
//        let uri = "waze://?ll=\(latitude),\(longitude)&navigate=yes"
//        let uri2 = "http://itunes.apple.com/us/app/id323229106"
//        if UIApplication.shared.canOpenURL(URL(fileURLWithPath: "waze://?ll=\(latitude),\(longitude)&navigate=yes")){
//            if let wazeUrl = URL(string: uri) {
//                UIApplication.shared.open(wazeUrl, options: [:], completionHandler: nil)
//            }
//        } else {
//            if let wazeUrl = URL(string: uri2) {
//                UIApplication.shared.open(wazeUrl, options: [:], completionHandler: nil)
//            }
//        }
        let location =  CLLocationCoordinate2D(latitude: 20.5756749, longitude: -100.3662626)
        if UIApplication.shared.canOpenURL(URL(string: "waze://")!) {
            // Waze is installed. Launch Waze and start navigation
            let urlStr: String = "waze://?ll=\(location.latitude),\(location.longitude)&navigate=yes"
            UIApplication.shared.open(URL(string: urlStr)!)
        }
        else {
            // Waze is not installed. Launch AppStore to install Waze app
            UIApplication.shared.open(URL(string: "http://itunes.apple.com/us/app/id323229106")!)
        }
    }
    // END [FUNCTION] for gps navigation
    // START [FUNCTION] for Apple Maps navigation
    func openMaps(){
        let latitude = Double("20.5754")!
        let longitude = Double("-100.3632")!
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let placemark = MKPlacemark(coordinate: coordinate)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Instituto Mexicano De Oftalmologia I.A.P."
        
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        mapItem.openInMaps(launchOptions: launchOptions)
    }
    // END [FUNCTION] for Apple Maps navigation
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        
        if segue.identifier == "homeAAjustesCuenta" {
            let destVC = segue.destination as! AccountViewController
            destVC.carnetDelUsuario = carnetDelUsuario
        }
        
        if segue.identifier == "HomeAAppointments" {
            let destVC = segue.destination as! AppointmentViewController
            destVC.carnetDelUsuario = carnetDelUsuario
        }
        
        if segue.identifier == "HomeAPreviousAppointments" {
            let destVC = segue.destination as! PreviousAppointmentViewController
            destVC.carnetDelUsuario = carnetDelUsuario
        }
        
        if segue.identifier == "VerMiCuentaAMiCuenta" {
            let destVC = segue.destination as! UserProfileController
            destVC.carnetDelUsuario = carnetDelUsuario
        }
        
        
    }
}
