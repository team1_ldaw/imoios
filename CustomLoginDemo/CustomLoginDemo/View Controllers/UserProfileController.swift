//
//  UserProfileController.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 02/11/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAuth
import FirebaseFirestore

class UserProfileController: UIViewController {
    
    var carnetDelUsuario : String!
    
    @IBOutlet weak var goBackButton: UIButton!
    
    @IBOutlet weak var correoLabel: UILabel!
    
    @IBOutlet weak var carnetLabel: UILabel!
    
    @objc func fetchUser() {
        if Auth.auth().currentUser != nil {
            guard (Auth.auth().currentUser?.uid) != nil else {
            print( "Error al cargar usuario.");
            return
          }
            
            self.correoLabel.text = Auth.auth().currentUser?.email
            self.carnetLabel.text = carnetDelUsuario
            
        
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchUser()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickGoBACK(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    /*
    fileprivate func setupViews() {
        view.addSubview(nameLabel)
        view.addSubview(carnetLabel)
    }*/
}
