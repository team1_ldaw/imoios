import UIKit
import AVKit
import Firebase

class ViewController: UIViewController {
    var carnetUsuario : String!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpElements()
        
        //print (carnetUsuario!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func setUpElements() {
        Utilities.styleFilledButton(signUpButton)
        Utilities.styleHollowButton(loginButton)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        /*
        let destSUVC = segue.destination as! SignUpViewController
        destSUVC.carnetDelUsuario = carnetUsuario!
        */
        
        if segue.identifier == "inicioARegistrarse" {
            let detailVC = segue.destination as! SignUpViewController
            detailVC.carnetDelUsuario = carnetUsuario!
        }
        
        if segue.identifier == "inicioALogin" {
            let detailVC = segue.destination as! LoginViewController
            detailVC.carnetDelUsuario = carnetUsuario!
        }
        
    }

}

