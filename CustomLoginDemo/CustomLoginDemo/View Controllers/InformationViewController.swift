//
//  InformationViewController.swift
//  CustomLoginDemo
//
//  Created by Alejandro Gleason on 10/16/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation

class InformationViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    @IBAction func didTappedBack(_ sender: Any){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func setupUI(){
        setupTitle()
    }
    
}
