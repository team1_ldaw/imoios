import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit
import FBSDKCoreKit
import FBSDKShareKit
import GoogleSignIn
import TwitterKit

class LoginViewController: UIViewController, LoginButtonDelegate, GIDSignInDelegate {
    
     var carnetDelUsuario : String!
    
    
    @IBOutlet weak var dismissButton: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewDidLoad() {
        setUpElements()
        super.viewDidLoad()
        print(carnetDelUsuario)
        setupTwitterButton()
        GIDSignIn.sharedInstance()?.presentingViewController = self

        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        GIDSignIn.sharedInstance().delegate = self
        
        let gSignIn = GIDSignInButton(frame: CGRect(x: 46, y: 400, width: view.frame.width - 88, height: 50))
        //gSignIn.center = view.center
        view.addSubview(gSignIn)
        
        // Do any additional setup after loading the view.
        
        let loginButton = FBLoginButton()
        loginButton.delegate = self
        
        view.addSubview(loginButton)

        loginButton.frame = CGRect(x: 46, y: 330, width: view.frame.width - 88, height: 50)
        
        if (AccessToken.current != nil) {
          // User is logged in, do work such as go to next view controller.
            /*let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.homeViewController) as? HomeViewController
            
            self.view.window?.rootViewController = homeViewController
            self.view.window?.makeKeyAndVisible()*/
            self.performSegue(withIdentifier: "IniciarSesionAHome", sender: nil)
        }
        /*
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)*/
    }
    /*
    @objc func hideKeyboard() {
        view.endEditing(true)
    }*/
    
    // Login with Twitter
        fileprivate func setupTwitterButton() {
            let twitterSignInButton = TWTRLogInButton(logInCompletion: { session, error in
                if (error != nil) {
                    print("Twitter authentication failed 1 : \(String(describing: error?.localizedDescription))")
                } else {
                    guard let token = session?.authToken else {return}
                    guard let secret = session?.authTokenSecret else {return}
                    let credential = TwitterAuthProvider.credential(withToken: token, secret: secret)
                    
                    Auth.auth().signIn(with: credential, completion: { (user, error) in
                        if error == nil {
                            print("Twitter authentication succeed")
                        } else {
                            print("Twitter authentication failed 2")
                        }
                      //let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.homeViewController) as! HomeViewController
//                        self.present(homeViewController, animated: true, completion: nil)
                        self.performSegue(withIdentifier: "IniciarSesionAHome", sender: nil)
                    })
                }
            })
            twitterSignInButton.frame = CGRect(x: 46, y: 470, width: view.frame.width - 88, height: 50)
            view.addSubview(twitterSignInButton)
        }
       //End Login with Twitter
    
   func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
         if let error = error {
           if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
             print("The user has not signed in before or they have since signed out.")
           } else {
             print("\(error.localizedDescription)")
           }
           return
         }
         // Perform any operations on signed in user here.
         let userId = user.userID                  // For client-side use only!
         let idToken = user.authentication.idToken // Safe to send to the server
         let fullName = user.profile.name
         let givenName = user.profile.givenName
         let familyName = user.profile.familyName
         let email = user.profile.email
         print(fullName)
         // User is logged in, do work such as go to next view controller.
    /*
    let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.homeViewController) as? HomeViewController
        
         self.view.window?.rootViewController = homeViewController
         self.view.window?.makeKeyAndVisible()*/
        self.performSegue(withIdentifier: "IniciarSesionAHome", sender: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        print("User has disconnected")
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
           if let error = error {
             print(error.localizedDescription)
             return
            }
        
        if let _ = AccessToken.current { /* If condicional que determina si hay algo dentro de la variable AccessToken.current, que es lo mismo de != nil */
            print(AccessToken.current!.tokenString)
            let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
            Auth.auth().signIn(with: credential) { (authResult, error) in
              if let error = error {
                // ...
                return
              }
              //redireccionar a home
              self.performSegue(withIdentifier: "IniciarSesionAHome", sender: nil)
                /*
              let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.homeViewController) as? HomeViewController
              
              self.view.window?.rootViewController = homeViewController
              self.view.window?.makeKeyAndVisible()*/
            }
            // ...
        }
        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
           // Do something when the user logout
           print("Logged out")
    }
    
    func setUpElements() {
        
        // Hide the error label
        errorLabel.alpha = 0
        
        // Style the elements
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(loginButton)
    }
    
    @IBAction func goBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        // TODO: Validate Text Fields
        
        // Create cleaned versions of the text field
        let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        // Signing in the user
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            
            if error != nil {
                // Couldn't sign in
                self.errorLabel.text = "El usuario y/o contraseña son incorrectos"
                self.errorLabel.alpha = 1
            }
            else if Auth.auth().currentUser != nil && !Auth.auth().currentUser!.isEmailVerified{
                let alert = UIAlertController(title: "¡El correo electrónico no ha sido verificado!", message: "Se recomienda verificar el correo electrónico para utilizar los servicios del IMO. Puede realizar esta acción desde la configuración de la cuenta.", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    //Redirect user to welcome page
                    self.performSegue(withIdentifier: "IniciarSesionAHome", sender: nil)
                }))

                self.present(alert, animated: true)
            }
            else {
                //Redirect user to welcome page
                self.performSegue(withIdentifier: "IniciarSesionAHome", sender: nil)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "IniciarSesionAHome" {
            let destVC = segue.destination as! HomeViewController
            destVC.carnetDelUsuario = carnetDelUsuario
        }
        
        if segue.identifier == "IniciarSesionAOlvide" {
            let destVC2 = segue.destination as! ForgotPasswordViewController
            destVC2.carnetDelUsuario = carnetDelUsuario
        }
        
    }
    
}
