//
//  SendSMSViewController.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 02/11/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation
import MessageUI

class SendSMSViewController: UIViewController, MFMessageComposeViewControllerDelegate {
    
    var msgText : String!
    
    var carnetDelUsuario : String!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var sendSMSButton: UIButton!
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
    }
    
    
    @IBAction func clickSendSMS(_ sender: Any) {
        if MFMessageComposeViewController.canSendText() {
            
            let controller = MFMessageComposeViewController()
            controller.body = self.msgText
            controller.recipients = [self.phoneTextField.text!]
            controller.messageComposeDelegate = self
            
            self.present (controller, animated: true, completion: nil)
        
        } else {
            print ("Cannot send text")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
}
