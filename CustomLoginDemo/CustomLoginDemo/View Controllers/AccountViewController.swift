//
//  AccountViewController.swift
//  CustomLoginDemo
//
//  Created by Alejandro Gleason on 9/28/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Firebase
import ProgressHUD

class AccountViewController: UIViewController {
    
    var carnetDelUsuario : String!
    
    /*Primero siempre hay que jalar cada item a la vista como outlet*/
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var emailContainer: UIStackView!
    @IBOutlet weak var emailVerification: UIButton!
    @IBOutlet weak var passContainer: UIStackView!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var resetPButton: UIButton!
    
    private var authUser : User? {
        return Auth.auth().currentUser
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    func setupUI(){
        setupEmailTextField()
        setupResetButton()
    }
    
    @IBAction func goBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetMailDidTapped(_ sender: UIButton) {
        switch sender.tag {
            case 1:
                guard let email = emailTextField.text, email != "" else{
                          ProgressHUD.showError("Favor de ingresar el correo electrónico.")
                          return
                          
                      }
                      resetMail(email: email, onSuccess: {
                          self.view.endEditing(true)
                          ProgressHUD.showSuccess("¡Tu correo ha sido restablecido! Te enviamos un mensaje de confirmación.")
                          self.navigationController?.popViewController(animated: true)
                      }) { (errorMessage) in
                          ProgressHUD.showError(errorMessage)
                      }
                break;
            default:
                break;
        }
       
    }
    
    func resetMail(email: String, onSuccess: @escaping() -> Void, onError: @escaping(_ errorMessage: String) -> Void){
        Auth.auth().currentUser?.updateEmail(to: email) { error in
            if error == nil {
                onSuccess()
            }else{
                onError(error!.localizedDescription)
            }
        }
    }
    // [START] email verification methods
    @IBAction func didTapSendSignInLink(_ sender: AnyObject) {
//        if self.authUser!.isEmailVerified{
//            emailVerification.isHidden = true;
//        }else{
//            sendVerificationMail()
//        }
        sendVerificationMail()
    }
    public func sendVerificationMail() {
        if self.authUser != nil && !self.authUser!.isEmailVerified {
            self.authUser!.sendEmailVerification(completion: { (error) in
                // Notify the user that the mail has sent or couldn't because of an error.
                print("¡Mail enviado!")
                let alert = UIAlertController(title: "¡Listo!", message: "Hemos enviado un enlace de verificación a tu correo electrónico.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: nil))
                self.present(alert, animated: true)
            })
        }
        else {
            // Either the user is not available, or the user is already verified.
            let alert = UIAlertController(title: "¡Atención!", message: "Tu correo electrónico ya ha sido verificado anteriormente, esta acción ya no es requerida :).", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    // [END] email verification methods
    @IBAction func resetPass(_ sender: UIButton) {
        switch sender.tag {
            case 2:
                guard let pass = passTextField.text, pass != "" else{
                    ProgressHUD.showError("Favor de ingresar la nueva contraseña.")
                    return
                    
                }
                resetPass(pass: pass, onSuccess: {
                    self.view.endEditing(true)
                    ProgressHUD.showSuccess("¡Tu contraseña ha sido restablecida! Te enviamos un mensaje de confirmación.")
                    self.navigationController?.popViewController(animated: true)
                }) { (errorMessage) in
                    ProgressHUD.showError(errorMessage)
                }
                break;
            default:
                break;
        }
    }
    
    func resetPass(pass: String, onSuccess: @escaping() -> Void, onError: @escaping(_ errorMessage: String) -> Void){
        Auth.auth().currentUser?.updatePassword(to: pass) { error in
            if error == nil {
                onSuccess()
            }else{
                onError(error!.localizedDescription)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
