//
//  DoctoresViewController.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 21/10/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation

class DoctoresViewController: UIViewController {
    @IBOutlet weak var goBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func clickGoBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
