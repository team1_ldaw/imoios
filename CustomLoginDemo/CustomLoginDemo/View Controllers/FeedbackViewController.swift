//
//  FeedbackViewController.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 10/11/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation

class FeedbackViewController: UIViewController {
    
    var URL_SAVE_FEEDBACK = "https://imocitas.000webhostapp.com/feedback.php"

    @IBOutlet weak var content: UITextView!
    
    @IBOutlet weak var feedbackTitle: UITextField!
    
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var goBackButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.content.layer.borderColor = UIColor(hue: 0, saturation: 1, brightness: 0.58, alpha: 1.0).cgColor
        self.content.layer.borderWidth = 1.0;
        self.content.layer.cornerRadius = 8;
        Utilities.styleHollowButton(sendButton)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func clickGoBack(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSendFeedback(_ sender: Any) {
        //created NSURL
        let requestURL = URL(string: URL_SAVE_FEEDBACK)
        
        //creating NSMutableURLRequest
        let request = NSMutableURLRequest(url: requestURL!)
        
        //setting the method to post
        request.httpMethod = "POST"
        
        //getting values from text fields
        let plataform = "iOS"
        let title = feedbackTitle.text!
        let contentToSend = content.text!
        
        if (title == "" && contentToSend == "") {
            let alert = UIAlertController(title: "Por favor llena todos los campos", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        //creating the post parameter by concatenating the keys and values from text field
        var postParameters: String!
        
        
        postParameters = "title="+title+"&content="+contentToSend+"&plataform="+plataform;
        
        //adding the parameters to request body
        request.httpBody = postParameters.data(using: String.Encoding.utf8)
        
        //creating a task to send the post request
        let task = URLSession.shared.dataTask(with: request as URLRequest){
            data, response, error in
            
            if error != nil{
                print("error is \(String(describing: error))")
                return;
            }
            
            //parsing the response
            do {
                //converting resonse to NSDictionary
                let myJSON =  try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject]
                
                //parsing the json
                if let parseJSON = myJSON {
                    
                    //creating a string
                    var msg : String!
                    
                    //getting the json response
                    msg = parseJSON["message"] as! String?
                    
                    
                    print(msg!)
                }
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Gracias", message: "Retroalimentación enviada de manera anónima", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            } catch {
                print(error)
            }
        }
        //executing the task
        task.resume()
    }
    
    
}
