//
//  File.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 10/6/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import MessageUI

class PreviousAppointmentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, MFMailComposeViewControllerDelegate, HomeModelProtocol  {
    
    let services = ["protesisOcular", "bajavision", "imagenologia","retina","cornea", "glaucoma", "oftalmo-pediatria"]
    
    @IBOutlet weak var esportAppointmentsButton: UIButton!
    
    @IBOutlet weak var enviarcorreo: UIButton!
    
    
    @IBOutlet weak var gBack: UIButton!
    
    @IBOutlet weak var listTableView: UITableView!
    
    var carnetDelUsuario : String!
    
    //Properties
    var feedItems: NSArray = NSArray()
    var selectedAppointment: AppointmentModel = AppointmentModel()
    
    var idAppointment: String!
    var citaPrevia: String!
    
    var appointmentArray: NSMutableArray = NSMutableArray()
 
     override func viewDidLoad() {
         super.viewDidLoad()
         setUpElements()
         //set delegates and initialize homeModel
         let homeModel = HomeModel()
         homeModel.delegate = self
         homeModel.downloadItems(carnet: carnetDelUsuario, previous: true)
     }
 
     func setUpElements() {
         // Style the elements
         Utilities.styleFilledButton(esportAppointmentsButton)
         Utilities.styleFilledButton(enviarcorreo)
     }
 
     func itemsDownloaded(items: NSArray) {
         feedItems = items
         appointmentArray = items as! NSMutableArray
         self.listTableView.reloadData()
     }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of feed items
        return feedItems.count
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         // Retrieve cell
        let cellIdentifier: String = "BasicCell"
        let myCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! Appointment2Cell
        // Get the appointment to be shown
        let item: AppointmentModel = feedItems[indexPath.row] as! AppointmentModel
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var enUSPOSIXLocale:NSLocale=NSLocale(localeIdentifier: "es")
        dateFormatter.locale=enUSPOSIXLocale as Locale
        //dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let strDate = dateFormatter.date(from: item.date!)
            
            dateFormatter.dateFormat = "EEEE, d MMM yyyy HH:mm a"
        dateFormatter.locale=enUSPOSIXLocale as Locale
        //dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let dateTxt = dateFormatter.string(from: strDate!)
        
        
            //myCell.textLabel!.text = item.idService_id
            myCell.imagen.image=UIImage(named: services[Int(item.idService_id!)!-1])
            myCell.fecha.text = dateTxt
        return myCell
    }
    
    
    @IBAction func enviarPorCorreo(_ sender: Any) {
        let fileName = "misCitasIMO.csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        var csvText = "Mis citas previas IMO\n (Fecha (año-mes-día | hora),Servicio\n"
        let count = feedItems.count
        
        if count > 0 {
            
            for i in 0..<(count) {
                let item: AppointmentModel = feedItems[i] as! AppointmentModel
                let newLine = "\(String(describing: item.date!)),\(String(describing: (services[Int(item.idService_id!)!-1])))\n"
                csvText.append(contentsOf: newLine)
            }
            do {
                
                try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
                               
                if MFMailComposeViewController.canSendMail() {
                    let emailController = MFMailComposeViewController()
                    emailController.mailComposeDelegate = self
                    emailController.setToRecipients([]) //I usually leave this blank unless it's a "message the developer" type thing
                    emailController.setSubject("Mi historial de citas IMO")
                    emailController.setMessageBody("Se adjunta su historial de citas IMO", isHTML: false)
                    
                    emailController.addAttachmentData(NSData(contentsOf: path!)! as Data, mimeType: "text/csv", fileName: "misCitasIMO.csv")
                                        
                    present(emailController, animated: true, completion: nil)
                }
                
            } catch {
                
                print("Failed to create file")
                print("\(error)")
            }
        } else {
            print ("Error: no hay datos a exportar")
        }
    }
    
    @IBAction func exportAppointments(_ sender: Any) {
        
        let fileName = "misCitasIMO.csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        var csvText = "Mis citas previas IMO\n (Fecha (año-mes-día | hora),Servicio\n"
        let count = feedItems.count
        
        if count > 0 {
            
            for i in 0..<(count) {
                let item: AppointmentModel = feedItems[i] as! AppointmentModel
                let newLine = "\(String(describing: item.date!)),\(String(describing: (services[Int(item.idService_id!)!-1])))\n"
                csvText.append(contentsOf: newLine)
            }
            do {
                
                try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
                
                let vc = UIActivityViewController(activityItems: [path], applicationActivities: [])
                vc.excludedActivityTypes = [
                    UIActivity.ActivityType.assignToContact,
                    UIActivity.ActivityType.saveToCameraRoll,
                    UIActivity.ActivityType.postToFlickr,
                    UIActivity.ActivityType.postToVimeo,
                    UIActivity.ActivityType.postToTencentWeibo,
                    UIActivity.ActivityType.postToTwitter,
                    UIActivity.ActivityType.postToFacebook,
                    UIActivity.ActivityType.openInIBooks
                ]
                present(vc, animated: true, completion: nil)
            } catch {
                
                print("Failed to create file")
                print("\(error)")
            }
        } else {
            print ("Error: no hay datos a exportar")
        }
        
    }
    
    private func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func GoToPrevious(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AppointmentANewAppointment" {
            let destVC = segue.destination as! AppointmentFormViewController
            destVC.carnetDelUsuario = carnetDelUsuario
            destVC.idAppointment = idAppointment
            destVC.citaPrevia = citaPrevia
            
        }
        
    }*/
    
}
