//
//  ForgotPasswordViewController+UI.swift
//  CustomLoginDemo
//
//  Created by Alejandro Gleason on 9/28/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit

/* Here I just define the design components */

extension ForgotPasswordViewController {
    
    func setupEmailTextField(){
        emailContainer.layer.borderWidth = 1
        emailContainer.layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor
        emailContainer.layer.cornerRadius = 3
        emailContainer.clipsToBounds = true
        
        //emailTextField.borderStyle = .none
        
        let placeholderAttr = NSAttributedString(string: "Correo Electrónico", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)])
        
        emailTextField.attributedPlaceholder = placeholderAttr
        emailTextField.textColor = UIColor(red: 99/255, green: 99/255, blue: 99/255, alpha: 1)
    }
    
    func setupResetButton(){
        resetButton.setTitle("RESTABLECER MI CONTRASEÑA", for: UIControl.State.normal)
        resetButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        resetButton.backgroundColor = UIColor(red:0.53, green:0.10, blue:0.06, alpha:1.0)
        resetButton.layer.cornerRadius = 5
        resetButton.clipsToBounds = true
        resetButton.setTitleColor(.white, for: UIControl.State.normal)
        
    }
}
