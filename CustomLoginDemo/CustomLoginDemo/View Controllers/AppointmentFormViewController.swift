//
//  AppointmentFormViewController.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 9/29/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import EventKit
import UserNotifications
import MessageUI

import ProgressHUD

class AppointmentFormViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, MFMailComposeViewControllerDelegate {
    
    var idService: Int = 0;
    
    
    let services = ["Prótesis ocular","Baja visión","Imagenología","Retina", "Segmento anterior & córnea", "Glaucoma", "Oftalmo pediatría"]
    
    @IBOutlet weak var service: UIPickerView!
    
    var savedEventId : String = ""
    
    //VARIABLES PARA MODIFICAR CITA
    var idAppointment: String!
    var citaPrevia: String!
    var msgText: String!
    var carnetDelUsuario : String!
    
    //let userID = Auth.auth().currentUser!.uid
    
    var URL_SAVE_APPOINTMENT = "https://imocitas.000webhostapp.com/api/checkAppointmentAvailabilityDos.php"
    
    @IBOutlet weak var inputDate: UITextField!
    
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
        inputDate.isUserInteractionEnabled = false
        //En caso de que
        if idAppointment != nil && citaPrevia != nil {
            URL_SAVE_APPOINTMENT = "https://imocitas.000webhostapp.com/reeschedule_service.php"
            inputDate.text = citaPrevia
        }
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler:  {didAllow, error in})
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return services.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        /*idService = row + 1
        print(idService)*/
        return services[row]
    }
    
    func setUpElements() {
        // Style the elements
        Utilities.styleTextField(inputDate)
        Utilities.styleFilledButton(confirmButton)
    }
    
    // iPHONE CALENDAR -- Creates an event in the EKEventStore. The method assumes the eventStore is created and accessible
    func createEvent(eventStore: EKEventStore, title: String, startDate: NSDate, endDate: NSDate) {
        
        let event = EKEvent(eventStore: eventStore)
        
        event.title = title
        event.startDate = startDate as Date
        event.endDate = endDate as Date
        event.calendar = eventStore.defaultCalendarForNewEvents
        
        do {
            try eventStore.save(event, span: .thisEvent)
            savedEventId = event.eventIdentifier
        } catch {
            print("Bad things happened")
        }
    }
    
    //Button action method
    @IBAction func confirmAppointment(_ sender: Any) {
        //created NSURL
        let requestURL = URL(string: URL_SAVE_APPOINTMENT)
        print("Click");
        //creating NSMutableURLRequest
        let request = NSMutableURLRequest(url: requestURL!)
        
        //setting the method to post
        request.httpMethod = "POST"
        
        //getting values from text fields
        let carnet = carnetDelUsuario
        let encoded = inputDate.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let date = encoded
        
        //creating the post parameter by concatenating the keys and values from text field
        var postParameters: String!
        //Si es un cambio de cita cambia la fecha usando su id
        idService = service.selectedRow(inComponent: 0)
        idService += 1
        let servicioString = String(idService)
        //print("idService")
        //print(idService)
        //print("servicioString")
        //print(servicioString)
        
        
        if idAppointment != nil && citaPrevia != nil { /*Si estoy haciendo un update*/
            postParameters = "idAppointment="+idAppointment!+"&date="+date!+"&service="+servicioString;
        }else{ //Si es una cita nueva 
            postParameters = "carnet="+carnet!+"&date="+date!+"&service="+servicioString;
        }
        
        //adding the parameters to request body
        request.httpBody = postParameters.data(using: String.Encoding.utf8)
        
        let eventStore = EKEventStore()

        // use date constant here
        let startDate =  datePicker.date //NSDate()
        let endDate = startDate.addingTimeInterval(60 * 60) // One hour
        
        //Creación del evento en APPLE CALENDAR
        if (EKEventStore.authorizationStatus(for: .event) != EKAuthorizationStatus.authorized) {
            eventStore.requestAccess(to: .event, completion: {
                granted, error in
                self.createEvent(eventStore: eventStore, title: "Cita IMO", startDate: startDate as NSDate, endDate: endDate as NSDate)
            })
        } else {
            createEvent(eventStore: eventStore, title: "Cita IMO", startDate: startDate as NSDate, endDate: endDate as NSDate)
        }
        
        //creating a string
        var msg : String!
        //creating a task to send the post request
        let task = URLSession.shared.dataTask(with: request as URLRequest){
            data, response, error in
            
            if error != nil{
                print("error is \(String(describing: error))")
                return;
            }
            
            //parsing the response
            do {
                //converting resonse to NSDictionary
                let myJSON =  try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject]
                
                //parsing the json
                if let parseJSON = myJSON {
                    //creating a string
                    
                    
                    
                    //getting the json response
                    msg = parseJSON["message"] as! String?
                    
                    
                    
                    DispatchQueue.main.async {
                        if (msg != "¡Cita creada exitosamente!"){
                            let alert = UIAlertController(title: msg, message: "", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            print(msg!)
                        }else{
                            let alert = UIAlertController(title: msg, message: "¿Desea reenviarse los datos de la cita?", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Por correo", style: .default, handler: { action in
                                    if MFMailComposeViewController.canSendMail() {
                                        let emailController = MFMailComposeViewController()
                                        emailController.mailComposeDelegate = self
                                        emailController.setToRecipients([]) //I usually leave this blank unless it's a "message the developer" type thing
                                        emailController.setSubject("Mi cita IMO")
                                        
                                        //formatear la fecha
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm"
                                        var enUSPOSIXLocale:NSLocale=NSLocale(localeIdentifier: "es")
                                        dateFormatter.locale=enUSPOSIXLocale as Locale
                                        
                                        let dateTxt = dateFormatter.string(from: self.datePicker.date)
                                        
                                        let info = "Su cita de: " + self.services[self.idService] + ", se programó para la fecha: " + dateTxt
                                        self.msgText = info
                                        emailController.setMessageBody(info, isHTML: false)
                                        self.present(emailController, animated: true, completion: nil)
                                    }
                                }))
                                alert.addAction(UIAlertAction(title: "Por SMS", style: .default, handler: { action in
                                    //formatear la fecha
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "E, d MMM yyyy HH:mm"
                                    var enUSPOSIXLocale:NSLocale=NSLocale(localeIdentifier: "es")
                                    dateFormatter.locale=enUSPOSIXLocale as Locale
                                    
                                    let dateTxt = dateFormatter.string(from: self.datePicker.date)
                                    
                                    let info = "Su cita de: " + self.services[self.idService] + ", se programó para la fecha: " + dateTxt
                                    self.msgText = info
                                    self.performSegue(withIdentifier: "CrearCitaASMS", sender: nil)
                                }))
                                alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                            print(msg!)
                        }
                    
                }
            } catch {
                print(error)
            }
            
        }
        //executing the task
        task.resume()
        
        if (msg == "¡Cita creada exitosamente!"){
        //NOTIFICACION
        /*Crear notificación*/
        // Step 1: Ask for permission
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
        }
        
        // Step 2: Create the notification content
        let content = UNMutableNotificationContent()
        content.title = "¡Tú cita se aproxima!"
        content.body = "Más información en la aplicación"
        
        // Step 3: Create the notification trigger
        let fecha = datePicker.date.addingTimeInterval(-30)
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: fecha)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        // Step 4: Create the request
        let uuidString = UUID().uuidString
        let req = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
        
        // Step 5: Register the request
        center.add(req) { (error) in
            // Check the error parameter and handle any errors
        }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func datePickerChanged(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd+HH:mm:ss"
        
        let enUSPOSIXLocale:NSLocale=NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.locale = enUSPOSIXLocale as Locale
        //dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        
        let strDate = dateFormatter.string(from: datePicker.date)
       
        inputDate.text = strDate
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        
        if segue.identifier == "CrearCitaASMS" {
            let destVC = segue.destination as! SendSMSViewController
            destVC.carnetDelUsuario = carnetDelUsuario
            destVC.msgText = msgText
        }
    }

}
