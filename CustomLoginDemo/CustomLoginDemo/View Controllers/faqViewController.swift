//
//  faqViewController.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 10/11/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation

class faqViewController: UIViewController {
    
    
    @IBOutlet weak var gobackButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickGoBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

}
