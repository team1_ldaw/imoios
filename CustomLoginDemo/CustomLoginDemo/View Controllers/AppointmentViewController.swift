//
//  File.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 9/29/19.
//  Copyright © 2019 Tony Vazquez. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import MessageUI

class AppointmentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, MFMailComposeViewControllerDelegate, HomeModelProtocol  {
    
    let services = ["protesisOcular", "bajavision", "imagenologia","retina","cornea", "glaucoma", "oftalmo-pediatria"]
    
    @IBOutlet weak var shareAppointmentsButton: UIButton!
    
    @IBOutlet weak var emailAppointmentsButton: UIButton!
    
    let URL_DELETE_APPOINTMENT = "https://imocitas.000webhostapp.com/delete_service.php"
    
    var carnetDelUsuario : String!
    var feedItems: NSMutableArray = NSMutableArray()
    var selectedAppointment: AppointmentModel = AppointmentModel()
    var idAppointment: String!
    var citaPrevia: String!
    var searching = false
    var appointmentArray: NSMutableArray = NSMutableArray()
    var arr: Array = Array<Any>() //arreglo auxiliar cuando buscan
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var listTableView: UITableView!
    
    @IBOutlet weak var newAppointmentButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
        let homeModel = HomeModel()
        homeModel.delegate = self
        homeModel.downloadItems(carnet: carnetDelUsuario, previous: false)
        self.listTableView.reloadData()
    }
    
    func setUpElements() {
        // Style the elements
        Utilities.styleFilledButton(newAppointmentButton)
        Utilities.styleFilledButton(shareAppointmentsButton)
        Utilities.styleFilledButton(emailAppointmentsButton)
    }
    
    func itemsDownloaded(items: NSArray) {
        feedItems = items as! NSMutableArray
        arr = items as! [Any]
        self.listTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of feed items
        if searching{
             return arr.count
        }else{
             return feedItems.count
        }
    }
    
    let dateFormatter = DateFormatter()
    
    func formatearFechaBonita(fechaFormatoBD:String!) -> String {
        
        let datos = fechaFormatoBD.split(separator: " ")
        let fechaAux = datos[0].split(separator: "-")
        let hora = datos[1].split(separator: ":")
        var aux = " "
        var horaAux = " "
        switch(fechaAux[1]){
            case "1":
                aux = "Enero"
            case "2":
                aux = "Febrero"
            case "3":
                aux = "Marzo"
            case "4":
                aux = "Abril"
            case "5":
                aux = "Mayo"
            case "6":
                aux = "Junio"
            case "7":
                aux = "Julio"
            case "8":
                aux = "Agosto"
            case "9":
                aux = "Septiembre"
            case "10":
                aux = "Octubre"
            case "11":
                aux = "Noviembre"
            case "12":
                aux = "Diciembre"
        default:
            print("Error al traducir el mes de la fecha")
        }
        let fechaAux1 = fechaAux[2] + " de " + aux + " del " + fechaAux[0]
        if(Int(String(hora[0]))! >= 12){
            if(Int(hora[0]) == 12){
                horaAux = fechaAux1 + " a las " + String(hora[0])+":"+hora[1]+" PM"
            }
            else{
                let horaAux1 = Int(String(hora[0]))! - 12
                horaAux = fechaAux1 + " a las " + String(horaAux1)+":"+hora[1]+" PM"
            }
        }
        else{
            horaAux = fechaAux1 + " a las " + hora[0]+":"+hora[1]+" AM"
        }
        return horaAux;
    }
    
    //Each cell is filled
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // Retrieve cell
       let cellIdentifier: String = "BasicCell"
       let myCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AppointmentCell
        
       // Get the appointment to be shown
       let item: AppointmentModel = feedItems[indexPath.row] as! AppointmentModel
       if searching {
            let item2: AppointmentModel = arr[indexPath.row] as! AppointmentModel
            
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            var enUSPOSIXLocale:NSLocale=NSLocale(localeIdentifier: "es")
            dateFormatter.locale=enUSPOSIXLocale as Locale
            //dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        
            let strDate = dateFormatter.date(from: item2.date!)
            
            dateFormatter.dateFormat = "EEEE, d MMM yyyy HH:mm a"
            dateFormatter.locale=enUSPOSIXLocale as Locale
            //dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let dateTxt = dateFormatter.string(from: strDate!)
            
        
            //myCell.textLabel!.text = item.idService_id
        
            myCell.imagen.image=UIImage(named: services[Int(item.idService_id!)!-1])
            myCell.fecha.text = dateTxt
       }else{
        
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            var enUSPOSIXLocale:NSLocale=NSLocale(localeIdentifier: "es")
            dateFormatter.locale=enUSPOSIXLocale as Locale
            //dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        
            let strDate = dateFormatter.date(from: item.date!)
            print(strDate )
            
            dateFormatter.dateFormat = "EEEE, d MMM yyyy HH:mm a"
            dateFormatter.locale=enUSPOSIXLocale as Locale
            //dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let dateTxt = dateFormatter.string(from: strDate!)
       
            print(dateTxt as Any)
       
        //myCell.textLabel!.text = item.idService_id
        myCell.fecha.text = dateTxt
        
        myCell.imagen.image=UIImage(named: services[Int(item.idService_id!)!-1])
        
        
       }
       // Get references to labels of cell
       return myCell
   }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        

        let deleteItem = UITableViewRowAction(style: .normal, title: "Eliminar") { action, index in
            //var item: AppointmentModel = self.feedItems[indexPath.row] as! AppointmentModel
            var item: AppointmentModel
            if (self.searching == false) {
                item = self.feedItems[indexPath.row] as! AppointmentModel
                
            } else {
                item = self.arr[indexPath.row] as! AppointmentModel
            }
            //tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            
            //created NSURL
            let requestURL = URL(string: self.URL_DELETE_APPOINTMENT)
            
            //creating NSMutableURLRequest
            let request = NSMutableURLRequest(url: requestURL!)
            
            //setting the method to post
            request.httpMethod = "POST"
            
            //getting values from text fields
            let idABorrar = item.idAppointment
            
            
            self.feedItems.removeObject(at: indexPath.row)
            self.listTableView.deleteRows(at: [indexPath], with: .automatic)
            
            //creating the post parameter by concatenating the keys and values from text field
            let postParameters = "idAppointment="+idABorrar!;
            
            //adding the parameters to request body
            request.httpBody = postParameters.data(using: String.Encoding.utf8)
            
            //creating a task to send the post request
            let task = URLSession.shared.dataTask(with: request as URLRequest){
                data, response, error in
                if error != nil{
                    print("error is \(String(describing: error))")
                    return;
                }
                //parsing the response
                do {
                    //converting resonse to NSDictionary
                    let myJSON =  try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject]
                    
                    //parsing the json
                    if let parseJSON = myJSON {
                        
                        //creating a string
                        var msg : String!
                        
                        //getting the json response
                        msg = parseJSON["message"] as! String?
                        
                        //printing the response
                        let alert = UIAlertController(title: msg, message: "Puede salir de esta ventana", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                              switch action.style{
                              case .default:
                                    print("default")

                              case .cancel:
                                    print("cancel")

                              case .destructive:
                                    print("destructive")
                        }}))
                        self.present(alert, animated: true, completion: nil)
                        print(msg!)
                    }
                } catch {
                    print(error)
                }
            }
            //executing the task
            task.resume()
            self.listTableView.reloadData()
        }
        deleteItem.backgroundColor = .red

        let modifyItem = UITableViewRowAction(style: .normal, title: "Cambiar Fecha") { action, index in
            var item: AppointmentModel
            if (self.searching == false) {
                item = self.feedItems[indexPath.row] as! AppointmentModel
            } else {
                item = self.arr[indexPath.row] as! AppointmentModel
            }
            self.idAppointment = item.idAppointment
            self.citaPrevia = item.date
            self.performSegue(withIdentifier: "AppointmentANewAppointment", sender: nil)
            
            let homeModel = HomeModel()
            homeModel.delegate = self
            homeModel.downloadItems(carnet: self.carnetDelUsuario, previous: false)
            self.listTableView.reloadData()
        }
        modifyItem.backgroundColor = .blue

        return [deleteItem, modifyItem]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    /*
    //Funcion para borrar o editar citas al deslizar
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //borrar
        let deleteItem = UIContextualAction(style: .destructive, title: "Eliminar") {  (contextualAction, view, boolValue) in
            
            //var item: AppointmentModel = self.feedItems[indexPath.row] as! AppointmentModel
            var item: AppointmentModel
            if (self.searching == false) {
                item = self.feedItems[indexPath.row] as! AppointmentModel
                
            } else {
                item = self.arr[indexPath.row] as! AppointmentModel
            }
            //tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            
            //created NSURL
            let requestURL = URL(string: self.URL_DELETE_APPOINTMENT)
            
            //creating NSMutableURLRequest
            let request = NSMutableURLRequest(url: requestURL!)
            
            //setting the method to post
            request.httpMethod = "POST"
            
            //getting values from text fields
            let idABorrar = item.idAppointment
            
            
            self.feedItems.removeObject(at: indexPath.row)
            self.listTableView.deleteRows(at: [indexPath], with: .automatic)
            
            //creating the post parameter by concatenating the keys and values from text field
            let postParameters = "idAppointment="+idABorrar!;
            
            //adding the parameters to request body
            request.httpBody = postParameters.data(using: String.Encoding.utf8)
            
            //creating a task to send the post request
            let task = URLSession.shared.dataTask(with: request as URLRequest){
                data, response, error in
                if error != nil{
                    print("error is \(String(describing: error))")
                    return;
                }
                //parsing the response
                do {
                    //converting resonse to NSDictionary
                    let myJSON =  try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject]
                    
                    //parsing the json
                    if let parseJSON = myJSON {
                        
                        //creating a string
                        var msg : String!
                        
                        //getting the json response
                        msg = parseJSON["message"] as! String?
                        
                        //printing the response
                        let alert = UIAlertController(title: msg, message: "Puede salir de esta ventana", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                              switch action.style{
                              case .default:
                                    print("default")

                              case .cancel:
                                    print("cancel")

                              case .destructive:
                                    print("destructive")
                        }}))
                        self.present(alert, animated: true, completion: nil)
                        print(msg!)
                    }
                } catch {
                    print(error)
                }
            }
            //executing the task
            task.resume()
            self.listTableView.reloadData()
        }
        
        let modifyItem = UIContextualAction(style: .normal, title: "Cambiar Fecha") {  (contextualAction, view, boolValue) in
            //let item: AppointmentModel = self.feedItems[indexPath.row] as! AppointmentModel
            var item: AppointmentModel
            if (self.searching == false) {
                item = self.feedItems[indexPath.row] as! AppointmentModel
            } else {
                item = self.arr[indexPath.row] as! AppointmentModel
            }
            self.idAppointment = item.idAppointment
            self.citaPrevia = item.date
            self.performSegue(withIdentifier: "AppointmentANewAppointment", sender: nil)
            
            let homeModel = HomeModel()
            homeModel.delegate = self
            homeModel.downloadItems(carnet: self.carnetDelUsuario, previous: false)
            self.listTableView.reloadData()
        }
        
        
        modifyItem.backgroundColor = UIColor(red:0.00, green:0.45, blue:0.74, alpha:1.0)
        let swipeActions = UISwipeActionsConfiguration(actions: [deleteItem, modifyItem])
        return swipeActions
    }*/
    
    @IBAction func goBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchText.isEmpty {
            searching = false
            //arr = feedItems as! [Any]
            listTableView.reloadData()
        } else{
            arr = feedItems.filter { (object: Any) -> Bool in
                if let casted = object as? AppointmentModel {
                    if(casted != nil) {
                        if((casted.date?.contains(searchText))!) {
                            return true
                        }
                    }
                }
                return false
            }
            searching = true
            //feedItems = arr as NSArray
            listTableView.reloadData()
        }
    }
    
    @IBAction func clickShareAppointments(_ sender: Any) {
        let fileName = "misCitasIMO.csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        var csvText = "Mis citas futuras IMO\n (Fecha (año-mes-día | hora),Servicio\n"
        var count = feedItems.count
        
        if searching{
             count = arr.count
        }else{
             count = feedItems.count
        }
        
        if count > 0 {
            
            for i in 0..<(count) {
                var item: AppointmentModel
                if searching {
                    item = arr[i] as! AppointmentModel
                }else{
                    item = feedItems[i] as! AppointmentModel
                }
                let newLine = "\(String(describing: item.date!)),\(String(describing: (services[Int(item.idService_id!)!-1])))\n"
                csvText.append(contentsOf: newLine)
            }
            do {
                
                try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
                
                let vc = UIActivityViewController(activityItems: [path as Any], applicationActivities: [])
                vc.excludedActivityTypes = [
                    UIActivity.ActivityType.assignToContact,
                    UIActivity.ActivityType.saveToCameraRoll,
                    UIActivity.ActivityType.postToFlickr,
                    UIActivity.ActivityType.postToVimeo,
                    UIActivity.ActivityType.postToTencentWeibo,
                    UIActivity.ActivityType.postToTwitter,
                    UIActivity.ActivityType.postToFacebook,
                    UIActivity.ActivityType.openInIBooks
                ]
                present(vc, animated: true, completion: nil)
            } catch {
                
                print("Failed to create file")
                print("\(error)")
            }
        } else {
            print ("Error: no hay datos a exportar")
        }
    }
    
    @IBAction func clickEmailAppointments(_ sender: Any) {
        let fileName = "misCitasIMO.csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        var csvText = "Mis citas futuras IMO\n (Fecha (año-mes-día | hora),Servicio\n"
        var count = feedItems.count
        
        if searching{
             count = arr.count
        }else{
             count = feedItems.count
        }
        
        if count > 0 {
            
            for i in 0..<(count) {
                var item: AppointmentModel
                if searching {
                    item = arr[i] as! AppointmentModel
                }else{
                    item = feedItems[i] as! AppointmentModel
                }
                let newLine = "\(String(describing: item.date!)),\(String(describing: (services[Int(item.idService_id!)!-1])))\n"
                csvText.append(contentsOf: newLine)
            }
            do {
                
                try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
                               
                if MFMailComposeViewController.canSendMail() {
                    let emailController = MFMailComposeViewController()
                    emailController.mailComposeDelegate = self
                    emailController.setToRecipients([]) //I usually leave this blank unless it's a "message the developer" type thing
                    emailController.setSubject("Mi historial de citas IMO")
                    emailController.setMessageBody("Se adjunta su historial de citas IMO", isHTML: false)
                    
                    emailController.addAttachmentData(NSData(contentsOf: path!)! as Data, mimeType: "text/csv", fileName: "misCitasIMO.csv")
                                        
                    present(emailController, animated: true, completion: nil)
                }
                
            } catch {
                
                print("Failed to create file")
                print("\(error)")
            }
        } else {
            print ("Error: no hay datos a exportar")
        }
    }
    
    private func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AppointmentANewAppointment" {
            let destVC = segue.destination as! AppointmentFormViewController
            destVC.carnetDelUsuario = carnetDelUsuario
            destVC.idAppointment = idAppointment
            destVC.citaPrevia = citaPrevia
        }
    }
}


