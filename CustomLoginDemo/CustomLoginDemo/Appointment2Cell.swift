//
//  AppointmentCell.swift
//  CustomLoginDemo
//
//  Created by Tony Vazquez on 10/6/19.
//  Copyright © 2019 Christopher Ching. All rights reserved.
//
import Foundation
import UIKit

class Appointment2Cell: UITableViewCell {
    
    @IBOutlet weak var imagen: UIImageView!
    
    @IBOutlet weak var fecha: UILabel!
    
    
    var idCita : String = ""

}
