import UIKit
import Firebase
import FBSDKCoreKit
import GoogleSignIn
import TwitterKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken devicetoken: Data){
        print("token :\(devicetoken)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    //var debug = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Twitter Login
        TWTRTwitter.sharedInstance().start(withConsumerKey: "gne9AzkYe9GpUXRrftvRTOrK4", consumerSecret: "ejMnCOPyxMkruI908K76DiVPioGWuJpDa8xZlQKP8ZaxD2uSs7")
        // Override point for customization after application launch.
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        GIDSignIn.sharedInstance().clientID = "462892993451-b826lodv7tuplaj6tq0inbr2eso5adf7.apps.googleusercontent.com"
        
        FirebaseApp.configure()
        /*
        if debug {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let next = storyboard.instantiateViewController(identifier: "HomeVC") as! HomeViewController
            let nav = UINavigationController(rootViewController: next)
            self.window?.rootViewController = nav

        }
        */
        UIApplication.shared.registerForRemoteNotifications()
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let googleAuthentication = GIDSignIn.sharedInstance().handle(url)
        let twitterLogin = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        return twitterLogin || googleAuthentication
    }
    
    /*func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled = ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        return handled
    }*/
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

